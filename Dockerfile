FROM alpine:edge
MAINTAINER Kolesár András <kolesar@kolesar.hu>

ARG http_proxy
ARG https_proxy

ENV http_proxy "$http_proxy"
ENV https_proxy "$https_proxy"

RUN echo http://dl-cdn.alpinelinux.org/alpine/edge/testing \
        >> /etc/apk/repositories && \
    apk --update add curl apache2 php7 php7-apache2 php7-mongodb php7-openssl php7-json php7-phar php7-iconv && \
    mkdir -p /run/apache2

# alpine composer package depends on php5
# mongodb is only available for php7
# therefore we install composer directly

RUN curl https://getcomposer.org/installer -o composer-setup.php && \
    php composer-setup.php --install-dir=/usr/local/bin --filename=composer && \
    php -r "unlink('composer-setup.php');"

WORKDIR /var/www/localhost/htdocs

# there is a dummy welcome page that needs to be removed
RUN rm index.html

# composer is invoked in two steps to enable caching for dependencies
COPY composer.* ./
RUN composer install --no-scripts --no-autoloader
COPY . ./
RUN composer dump-autoload --optimize

EXPOSE 80
CMD httpd -D FOREGROUND
