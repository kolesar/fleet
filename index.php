<?php

require 'vendor/autoload.php';
date_default_timezone_set('Europe/Budapest');

$client = new MongoDB\Client("mongodb://mongo:27017");

if (isset($_REQUEST['lat'])) {

    $collection = $client->fleet->points;
    $document = $_REQUEST;
    $document['received'] = (int) (microtime(true)*1000);
    $result = $collection->insertOne($document);
    $id = $result->getInsertedId();

    $collection = $client->fleet->last;
    $document = [
        'device' => $document['device'],
        'last' => $id
    ];
    $previous = $collection->findOneAndReplace(
        ['device' => $document['device']],
        $document
    );

    if ($previous === null) $collection->insertOne($document);

    header('Content-type: text/plain; charset=UTF-8');
    echo 'OK';
    exit;

} else if (isset($_REQUEST['track'])) {

    $collection = $client->fleet->last;
    $devices = $collection->find();
    $features = [];
    foreach ($devices as $device) {
        $features[$device->device] = [
            "type" => "Feature",
            "geometry" => [
                "type" => "LineString",
                "coordinates" => [],
            ],
        ];
    }

    $collection = $client->fleet->points;
    $result = $collection->find();

    foreach ($result as $point) {
        if (@$point->lat === null || @$point->lon === null) continue;
        if (!isset($features[$point->device])) continue;
        $features[$point->device]['geometry']['coordinates'][] = [
            (float) $point->lon,
            (float) $point->lat
        ];
    }
    header('Content-type: application/json; charset=UTF-8');
    echo json_encode([
        "type" => "FeatureCollection",
        "features" => array_values($features)
    ], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    exit;

} else if (isset($_REQUEST['last'])) {

    $collection = $client->fleet->last;
    $devices = $collection->find();
    $features = [];
    foreach ($devices as $device) {
        if (is_null($device->device)) continue;
        $collection = $client->fleet->points;
        $point = $collection->findOne(['_id' => $device->last]);
        if (@$point->lat === null || @$point->lon === null) continue;
        $features[] = [
            "type" => "Feature",
            "geometry" => [
                "type" => "Point",
                "coordinates" => [
                    (float) $point->lon,
                    (float) $point->lat
                ],
            ],
            "properties" => [
                "device" => $device->device
            ]
        ];
    }
    header('Content-type: application/json; charset=UTF-8');
    echo json_encode([
        "type" => "FeatureCollection",
        "features" => $features,
    ], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    exit;

}

$collection = $client->fleet->last;
$result = $collection->findOne(['device' => ['$ne' => null]]);
$collection = $client->fleet->points;
$result = $collection->findOne(['_id' => $result->last]);

?>
<!DOCTYPE html>
<html>
<head>

    <title>fleet</title>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.3/dist/leaflet.css" />
    <script src="https://unpkg.com/leaflet@1.0.3/dist/leaflet.js"></script>
    <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="https://use.fontawesome.com/b6697554e1.js"></script>

    <style>
        body {
            padding: 0;
            margin: 0;
        }
        html, body, #map {
            width: 100%;
            height: 100%;
        }
        #status {
            position: absolute;
            left: 0;
            bottom: 0px;
            z-index: 1000;
            font-family: "Helvetica Neue",Arial,Helvetica,sans-serif;
            font-size: 14px;
            color: #333;
        }
        .box {
            background-color: rgba(255, 255, 255, 0.7);
            padding: 10px;
            box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.65);
            border-radius: 4px;
            display: inline-block;
            margin-left: 10px;
            margin-bottom: 10px;
        }
        .battery-icon {
            font-size: 40px;
        }
        .battery-percentage {
            font-size: 18px;
            color: #555;
            margin-bottom: 3px;
        }
    </style>

</head>
<body>

<div id="map"></div>

<script>
    var coord = [<?php
        echo sprintf('%f, %f', $result->lat, $result->lon);
        ?>];

    var osm = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap<\/a> közreműködők'
    });
    var turistautak = L.tileLayer('http://{s}.tile.openstreetmap.hu/turistautak/{z}/{x}/{y}.png', {
        subdomains: ['h', 'i', 'j'],
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap<\/a> közreműködők'
    });
    var orto2000 = L.tileLayer('http://e.tile.openstreetmap.hu/ortofoto2000/{z}/{x}/{y}.jpg', {
        attribution: '&copy; FÖMI'
    });
    var orto2005 = L.tileLayer('http://e.tile.openstreetmap.hu/ortofoto2005/{z}/{x}/{y}.jpg', {
        attribution: '&copy; FÖMI'
    });

    var measurements = L.tileLayer('http://{s}.map.turistautak.hu/tiles/measurements/{z}/{x}/{y}.png', {
        subdomains: ['a', 'b', 'c'],
        attribution: '&copy; <a href="http://osm.org/copyright">OpenCellID<\/a> contributors CC-BY-SA 3.0'
    });

    var baselayers = {
        'OpenStreetMap (general)': osm,
        'OpenStreetMap (hiking)': turistautak,
        'orto 2000': orto2000,
        'orto 2005': orto2005
    };

    var overlays = {
        'measurements': measurements
    };

    var map = L.map('map', { layers: osm }).setView(coord, 12);

    var geojsonMarkerOptions = {
        radius: 5,
        fillColor: "#ff7800",
        color: "#000",
        weight: 2,
        opacity: 1,
        fillOpacity: 0.8
    };

    function addPointsToMap(data, map) {
        var dataLayer = L.geoJson(data, {
                pointToLayer: function (feature, latlng) {
                    return L.circleMarker(latlng, geojsonMarkerOptions);
                }
            });
        dataLayer.addTo(map);
    }

    function addTrackToMap(data, map) {
        var dataLayer = L.geoJson(data);
        dataLayer.addTo(map);
    }

    $.getJSON("?last", function(data) { addPointsToMap(data, map); });
    $.getJSON("?track", function(data) { addTrackToMap(data, map); });
    L.control.layers(baselayers, overlays).addTo(map);

</script>

<div id="status">
<?php
    $collection = $client->fleet->last;
    $devices = $collection->find();
    foreach ($devices as $device) {
        if (is_null($device->device)) continue;
        $collection = $client->fleet->points;
        $point = $collection->findOne(['_id' => $device->last]);
        if (@$point->lat === null || @$point->lon === null) continue;
?>
    <div class="box">
        <div title="battery" class="battery">
            <div><i class="battery-icon fa fa-battery-<?php
                if ($point->battery > 80) {
                    echo 'full';
                } else if ($point->battery > 60) {
                    echo 'three-quarters';
                } else if ($point->battery > 40) {
                    echo 'half';
                } else if ($point->battery > 20) {
                    echo 'quarter';
                } else {
                    echo 'empty';
                }
            ?>" aria-hidden="true"></i></div>
            <div class="battery-percentage"><?php
                echo $point->battery;
            ?> %</div>
        </div>
        <div title="speed" class="device"><?php
            echo $point->device;
        ?></div>
        <div title="date" class="date"><?php
            echo date('Y.m.d', $point->received/1000);
        ?></div>
        <div title="time" class="time"><?php
            echo date('H:i:s', $point->received/1000);
            $minutes = (int) floor((time()-$point->received/1000)/60);
            if ($minutes >= 1) echo sprintf(" (%d')", $minutes);
        ?></div>
        <div title="speed" class="speed"><?php
            echo sprintf('%0.0f', $point->speed * 3.6);
        ?> km/h</div>
    </div>
<?php
    }
?>
</div>
</body>
</html>
